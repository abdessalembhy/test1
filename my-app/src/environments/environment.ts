// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyA8UXr15osNJVM1-TL3519s36rAUNxvXos",
    authDomain: "mail-contact-8536c.firebaseapp.com",
    databaseURL: "https://mail-contact-8536c.firebaseio.com",
    projectId: "mail-contact-8536c",
    storageBucket: "mail-contact-8536c.appspot.com",
    messagingSenderId: "546219553046"
 }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
