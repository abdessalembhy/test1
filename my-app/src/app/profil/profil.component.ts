import { Component, OnInit,ViewEncapsulation } from '@angular/core';
import {NgbCarouselConfig} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [NgbCarouselConfig] 

})
export class ProfilComponent implements OnInit {
  public x ="hide";
  public y ="hide";
  public z ="hide";
  public hello="visible";


  showHide1(){
    if(this.x==="show"){
      this.x="hide";
    }
    else{
      this.x="show";
      this.hello="hidden";
    }
  }
  showHide2(){
    if(this.y==="show"){
      this.y="hide";
      
    }
    else{
      this.y="show";
      this.hello="hidden";
    }
  }
  showHide3(){
    if(this.z==="show"){
      this.z="hide";
    }
    else{
      this.z="show";
      this.hello="hidden";
    }
  }
  ngOnInit() {
  }

}
