import { Component, OnInit,ViewEncapsulation } from '@angular/core';
import  { FormGroup, FormBuilder, Validators } from  '@angular/forms'; 
import * as firebase from 'firebase';
import {DatePipe} from '@angular/common'

var firebaseConfig = {
  apiKey: "AIzaSyA8UXr15osNJVM1-TL3519s36rAUNxvXos",
  authDomain: "mail-contact-8536c.firebaseapp.com",
  databaseURL: "https://mail-contact-8536c.firebaseio.com",
  projectId: "mail-contact-8536c",
  storageBucket: "mail-contact-8536c.appspot.com",
  messagingSenderId: "546219553046",
  appId: "1:546219553046:web:828c11cd5f1bbdb8"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
var messageRef = firebase.database().ref('messages');

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css'],
  encapsulation: ViewEncapsulation.None
  
})

export class ContactComponent implements OnInit {
  today: number = Date.now();
  sended=false;
  MesForm: FormGroup;
  
  constructor(
    private formBuilder: FormBuilder,
    
    ) {
      this.createForm();

    }
    

    
    ngOnInit() {
    }
  private createForm() {
    this.MesForm = this.formBuilder.group({
      name: [null, Validators.compose([Validators.required, Validators.minLength(3)])],
      email: ['', Validators.required],
      message: [null, Validators.compose([Validators.required, Validators.minLength(10)])],
    });
  }
  saveMessage() {
    this.sended=true;
    var newMessageRef = messageRef.push();
    newMessageRef.set({
      name : this.MesForm.value['name'],
      email: this.MesForm.value['email'],
      message: this.MesForm.value['message'],
      date : this.today
    }) 
  }


}